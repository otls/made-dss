<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('result_id', false, true);
            $table->string('rslt_dt_alt', 255);
            $table->double('rslt_dt_value', 15, 6);
            $table->integer('rslt_dt_rank')->unsigned();
            $table->timestamps();

            $table->foreign('result_id', 'result_details_result_id_foreign')->references('id')->on('results')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('result_details', function (Blueprint $table) {
           $table->dropForeign('result_details_result_id_foreign');
        });
        Schema::dropIfExists('result_details');
    }
}
