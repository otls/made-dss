<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNormalitationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('normalization_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('normalization_id', false, true)->index();
            $table->bigInteger('criteria_id', false, true)->index();
            $table->float('nrmlzn_value')->default(0);
            $table->string('nrmlzn_label', 200)->nullable();
            $table->timestamps();

            $table->foreign('normalization_id')->references('id')->on('normalizations')->onDelete('cascade');
            $table->foreign('criteria_id')->references('id')->on('criterias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('normalization_details');
    }
}
