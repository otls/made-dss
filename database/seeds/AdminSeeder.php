<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'name' => 'M Admin',
            'username' => 'm-admin',
            'email' => 'm.admin@mail.cc',
            'password' => Hash::make('m-admin123')
        ];

        User::updateOrCreate(
            ['username' => $admin['username'], 'email' => $admin['email']],
            $admin
        );
    }
}
