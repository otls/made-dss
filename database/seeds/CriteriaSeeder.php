<?php

use App\Models\Criteria;
use Illuminate\Database\Seeder;

class CriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = config('made_dss.criteria');
        foreach ($criteria as $key => $value) {
            // dd($key);
            Criteria::updateOrCreate(
                ['crt_name' => $key, 'crt_weight' => $value[0]['weight'], 'crt_type' => $value[0]['type']],
                ['crt_name' => $key, 'crt_weight' => $value[0]['weight'], 'crt_type' => $value[0]['type'], 'crt_desc' => $value[0]['desc']],
            );
        }
    }
}
