<?php

use App\Models\Alternative;
use Illuminate\Database\Seeder;

class AlternativesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alternatives = config('made_dss.alternatives');
        // dd($alternatives);
        foreach ($alternatives as $key => $value) {
            Alternative::updateOrCreate(
                ['alt_email' => $value['alt_email'], 'alt_phone' => $value['alt_phone']],
                $value
            );
        }
    }
}
