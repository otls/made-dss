<?php

return [
    'criteria' => [
        'Harga' => [
            ['min' => 1, 'max' => 99999, 'value' => 1, 'unit' => 'IDR', 'weight' => '80', 'type' => '0', 'desc' => 'Harga dari buah merah'],
            ['min' => 100000, 'max' => 129999, 'value' => 2, 'unit' => 'IDR', 'weight' => '80', 'type' => '0', 'desc' => 'Harga dari buah merah'],
            ['min' => 130000, 'max' => 149999, 'value' => 3, 'unit' => 'IDR', 'weight' => '80', 'type' => '0', 'desc' => 'Harga dari buah merah'],
            ['min' => 150000, 'max' => 50000000000000, 'value' => 4, 'unit' => 'IDR', 'weight' => '80', 'type' => '0', 'desc' => 'Harga dari buah merah'],
        ],
        'Berat' => [
            ['min' => 1, 'max' => 6.9, 'value' => 1, 'unit' => 'Kg', 'weight' => '90', 'type' => '1', 'desc' => 'Berat Dari buah merah'],
            ['min' => 7, 'max' => 7.9, 'value' => 2, 'unit' => 'Kg', 'weight' => '90', 'type' => '1', 'desc' => 'Berat Dari buah merah'],
            ['min' => 8, 'max' => 8.9, 'value' => 3, 'unit' => 'Kg', 'weight' => '90', 'type' => '1', 'desc' => 'Berat Dari buah merah'],
            ['min' => 9, 'max' => 500000000000, 'value' => 4, 'unit' => 'Kg', 'weight' => '90', 'type' => '1', 'desc' => 'Berat Dari buah merah'],
        ],
        'Lebar' => [
            ['min' => 1, 'max' => 27.9, 'value' => 1, 'unit' => 'cm', 'weight' => '70', 'type' => '1', 'desc' => 'Lebar dari buah merah'],
            ['min' => 28, 'max' => 29.9, 'value' => 2, 'unit' => 'cm', 'weight' => '70', 'type' => '1', 'desc' => 'Lebar dari buah merah'],
            ['min' => 30, 'max' => 34.9, 'value' => 3, 'unit' => 'cm', 'weight' => '70', 'type' => '1', 'desc' => 'Lebar dari buah merah'],
            ['min' => 35, 'max' => 500000000000000, 'value' => 4, 'unit' => 'cm', 'weight' => '70', 'type' => '1', 'desc' => 'Lebar dari buah merah'],
        ],
        'Panjang' => [
            ['min' => 1, 'max' => 49.9, 'value' => 1, 'unit' => 'cm', 'weight' => '60', 'type' => '1', 'desc' => 'Panjang dari buah merah'],
            ['min' => 50, 'max' => 79.9, 'value' => 2, 'unit' => 'cm', 'weight' => '60', 'type' => '1', 'desc' => 'Panjang dari buah merah'],
            ['min' => 80, 'max' => 99.9, 'value' => 3, 'unit' => 'cm', 'weight' => '60', 'type' => '1', 'desc' => 'Panjang dari buah merah'],
            ['min' => 100, 'max' => 50000000000000, 'value' => 4, 'unit' => 'cm', 'weight' => '60', 'type' => '1', 'desc' => 'Panjang dari buah merah'],
        ],
    ],
    'alternatives' => [
        [
            "id" => 1,
            "alt_name" => "Bapak Yakobus",
            "alt_phone" => "081358310243",
            "alt_email" => "yakob56@gmail.com",
            "alt_address" => "sentani hawai",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ],
        [
            "id" => 2,
            "alt_name" => "Bapak Aldo",
            "alt_phone" => "085354415770",
            "alt_email" => "Aldo862@gmail.com",
            "alt_address" => "abepura jayapura",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ],
        [
            "id" => 3,
            "alt_name" => "Bapak Dhuha",
            "alt_phone" => "082344569786",
            "alt_email" => "Dhuha@yahoo.com",
            "alt_address" => "jalan baru pasar lama",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ],
        [
            "id" => 4,
            "alt_name" => "Bapak dominggus",
            "alt_phone" => "082198388770",
            "alt_email" => "dom60@gmail.com",
            "alt_address" => "sentany depapre",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ],
        [
            "id" => 5,
            "alt_name" => "Bapak Ghio",
            "alt_phone" => "082198379221",
            "alt_email" => "pradana@yahoo.co.id",
            "alt_address" => "CV Thomas entrop",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ],
        [
            "id" => 6,
            "alt_name" => "Bapak ipang",
            "alt_phone" => "082272045837",
            "alt_email" => "ipangm36@gmail.com",
            "alt_address" => "Tanjakan RS Bhayangkara Kotaraja",
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ]
    ]
];
