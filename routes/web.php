<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'web']], function () {
    Route::post('/alternative/data', 'AlternativeController@data')->name('alternative.data');
    Route::resource('/alternative', 'AlternativeController');

    Route::post('/criteria/data', 'CriteriaController@data')->name('criteria.data');
    Route::resource('/criteria', 'CriteriaController');

    Route::post('/selection/data', 'SelectionController@data')->name('selection.data');
    Route::resource('/selection', 'SelectionController');

    Route::put('/count-result', 'SelectionController@count')->name('count');
    Route::post('/save-result', 'SelectionController@save_results')->name('result_save');
});
