@extends('layouts.app')

@section('title')
    Criteria
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Kriteria</th>
                                <th>Bobot</th>
                                <th>Jenis</th>
                                <th width="45%">Keterangan</th>
                                <th class="d-none" width="15%"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        let datatable = null;
        let _token = $('meta[name=csrf-token]').attr('content');
        $(() => {
            $('.table').DataTable({
                serverSide: true,
                processing: true,
                ordering: true,
                paging: true,
                searching:true,
                autoWidth:true,
                ajax: {
                    type: 'POST',
                    url: `{{route('criteria.data')}}`,
                    data: {_token: _token}
                },
                columns: [
                    {
                        name: 'criterias.crt_name',
                        data: 'crt_name',
                        className: 'align-middle',
                    },
                    {
                        className: 'align-middle',
                        data: 'crt_weight',
                        name: 'criterias.crt_weight'
                    },
                    {
                        className: 'align-middle',
                        data: 'type',
                        name: 'criterias.crt_type',
                    },
                    {
                        className: 'align-middle',
                        data: 'crt_desc',
                        name: 'criterias.crt_desc'
                    },
                    {
                        className: 'align-middle d-none',
                        name: 'id',
                        data: 'id',
                        render: (value, a, data) => {
                            let urlEdit = `{{route('criteria.edit', ['criterion' => ':id'])}}`.replace(':id',`${value}`);
                            let urlDestroy = `{{route('criteria.destroy', ['criterion' => ':id'])}}`.replace(':id', `${value}`);
                            return `
                            <div class="row d-flex justify-content-center">
                                <a class="btn btn-sm btn-warning shadow-sm mr-2 mt-2 text-light" href="${urlEdit}"><i class="fas fa-edit"></i></a>
                                <form action="${urlDestroy}" method="POST" class="delete-confirm">
                                    <input type="hidden" value="${_token}" name="_token" />
                                    <input type="hidden" value="DELETE" name="_method" />
                                    <button type="submit" class="btn btn-sm btn-danger shadow-sm mr-2 mt-2 delete-confirm text-light"><i class="fas fa-trash"></i></a>
                                </form>
                            </div>

                            `;
                        }

                    },
                ],
                order: [[4, 'asc']]
                // drawCallback: () => {
                //     $(".datatable-add-button").remove();
                //     $('#DataTables_Table_0_filter').prepend(`
                //         <a  class='btn btn-info btn-sm mr-2 datatable-add-button' href="{{route('criteria.create')}}"><i class='fa fa-plus'></i> Tambah</a>
                //     `);
                //     setFormDeleteConfirmListener();
                // }
            });
        })
    </script>
@endsection
