@extends('layouts.app')

@section('title')
    Manajemen Alternatif
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Nama</th>
                                <th>No. Telepon</th>
                                <th>Email</th>
                                <th width="45%">Alamat</th>
                                <th width="15%"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        let datatable = null;
        let _token = $('meta[name=csrf-token]').attr('content');
        $(() => {
            $('.table').DataTable({
                serverSide: true,
                processing: true,
                ordering: true,
                paging: true,
                searching:true,
                autoWidth:true,
                ajax: {
                    type: 'POST',
                    url: `{{route('alternative.data')}}`,
                    data: {_token: _token}
                },
                columns: [
                    {
                        name: 'alt_name',
                        data: 'alt_name',
                        className: 'align-middle',
                        render: (value, a, data) => {
                            let edit = `{{route('alternative.edit', ['alternative' => ':id'])}}`.replace(':id', data.id);
                            return `<a href="${edit}">${value}</a>`;
                        }
                    },
                    {
                        className: 'align-middle',
                        name: 'alt_phone',
                        data: 'alt_phone'
                    },
                    {
                        className: 'align-middle',
                        name: 'alt_email',
                        data: 'alt_email'
                    },
                    {
                        className: 'align-middle',
                        name: 'alt_address',
                        data: 'alt_address'
                    },
                    {
                        className: 'align-middle',
                        name: 'id',
                        data: 'id',
                        render: (value, a, data) => {
                            let urlEdit = `{{route('alternative.edit', ['alternative' => ':id'])}}`.replace(':id',`${value}`);
                            let urlDestroy = `{{route('alternative.destroy', ['alternative' => ':id'])}}`.replace(':id', `${value}`);
                            return `
                            <div class="row d-flex justify-content-center">
                                <a class="btn btn-sm btn-warning shadow-sm mr-2 mt-2 text-light" href="${urlEdit}"><i class="fas fa-edit"></i></a>
                                <form action="${urlDestroy}" method="POST" class="delete-confirm">
                                    <input type="hidden" value="${_token}" name="_token" />
                                    <input type="hidden" value="DELETE" name="_method" />
                                    <button type="submit" class="btn btn-sm btn-danger shadow-sm mr-2 mt-2 delete-confirm text-light"><i class="fas fa-trash"></i></a>
                                </form>
                            </div>

                            `;
                        }
                    },
                ],
                drawCallback: () => {
                    $(".datatable-add-button").remove();
                    $('#DataTables_Table_0_filter').prepend(`
                        <a  class='btn btn-info btn-sm mr-2 datatable-add-button' href="{{route('alternative.create')}}"><i class='fa fa-plus'></i> Tambah</a>
                    `);
                    setFormDeleteConfirmListener();
                }
            });
        })
    </script>
@endsection
