<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <title>@yield('title', config('app.name')) | {{config('app.name')}}</title>

    <script src="{{asset('js/app.js')}}"></script>
   <link rel="stylesheet" href="{{asset('css/app.css')}}">
   <!-- Google Font: Source Sans Pro -->
   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav pace-white">
    @include('utilities.notification')
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-dark navbar-pink">
    <div class="container-fluid">
      <a href="{{route('home')}}" class="navbar-brand">
        {{-- <img src="{{asset('images/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3 bg-white"> --}}
        <span class="brand-text font-weight-light">{{config('app.name')}}</span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link @if(\Request::is('home')) active @endif">Beranda</a>
          </li>
          <li class="nav-item">
            <a href="{{route('alternative.index')}}" class="nav-link @if(\Request::is('alternative*')) active @endif">Alternatif</a>
          </li>
          <li class="nav-item">
            <a href="{{route('criteria.index')}}" class="nav-link @if(\Request::is('criteria*')) active @endif">Kriteria</a>
          </li>
          <li class="nav-item">
            <a href="{{route('selection.index')}}" class="nav-link @if(\Request::is('selection*')) active @endif">Pemilihan</a>
          </li>
        </ul>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item">
            <form action="{{route('logout')}}" method="post">
                @csrf
                <button class="nav-link btn">Logout</button>
            </form>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">@yield('title', config('app.name'))</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        @yield('content', 'It should be a content')
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Made DSS V.1.0.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020 <a href="https://www.facebook.com/profile.php?id=100009025596335" target="_blank">Tietan Chadell Unch</a>.</strong> All rights reserved.
  </footer>
</div>
</body>
</html>
