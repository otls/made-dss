@extends('layouts.app')

@section('title')
    Hasil Perhitungan Metode Moora
@endsection

@section('content')
    <div class="row" id="top">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Tabel Kriteria</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <th>Kriteria</th>
                            <th>Jenis</th>
                            <th>Bobot</th>
                        </thead>
                        <tbody>
                            @foreach ($criteria as $item)
                            <tr>
                                <td>{{$item->crt_name}}</td>
                                <td>{{$item->crt_type == '1' ? 'Benefit' : 'Cost'}}</td>
                                <td>{{$item->crt_weight}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Tabel Pencocokan</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Alt.</th>
                               @foreach ($criteria as $item)
                                   <th class="header-th text-center" data-name="{{strtolower(str_replace(" ", "_", $item->crt_name))}}">{{$item->crt_name}}</th>
                               @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alternatives as $item)
                                <tr>
                                    <td>{{$item->alt_name}}</td>
                                    @foreach ($item->normalization->detail as $val)
                                        <td class="text-center">{{$val->label}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Konversi Tabel Pencocokan</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Alt.</th>
                               @foreach ($criteria as $item)
                                   <th class="header-th text-center" data-name="{{strtolower(str_replace(" ", "_", $item->crt_name))}}">{{$item->crt_name}}</th>
                               @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alternatives as $item)
                                <tr>
                                    <td>{{$item->alt_name}}</td>
                                    @foreach ($item->normalization->detail as $val)
                                        <td class="text-center">{{$val->nrmlzn_value}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Tabel Normalisasi Matrix</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Alt.</th>
                               @foreach ($criteria as $item)
                                   <th class="header-th text-center" data-name="{{strtolower(str_replace(" ", "_", $item->crt_name))}}">{{$item->crt_name}}</th>
                               @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($normalizedMatrix as $item => $mtx)
                                <tr>
                                    <td>{{$item}}</td>
                                    @foreach ($mtx as $val)
                                        <td class="text-center">{{number_format($val, 4, ',', '.')}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Tabel Hasil Menghitung Nilai Optimasi</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Altternative</th>
                                <th>Nilai Optimasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{$item['alt_name']}}</td>
                                    <td>{{number_format($item['value'], 4, ',', '.')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <form action="{{route('result_save')}}" method="post">
                @csrf
                <div class="card border-0 shadow w-100">
                    <div class="card-body">
                        <h2 class="card-title font-weight-bold mb-3">Tabel Hasil Perankingan</h2>
                        <table class="table table-hover" id="ranking">
                            <thead class="">
                                <tr>
                                    <th>Ranking</th>
                                    <th>Altternative</th>
                                    <th>Nilai</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $data = collect($data)->sortByDesc('value');

                                @endphp
                                @foreach ($data as $key => $item)
                                <tr>
                                        <td>#{{$i++}}</td>
                                        <td>{{$item['alt_name']}}</td>
                                        <td>{{number_format($item['value'], 4, ',', '.')}}</td>
                                        <td><input type="hidden" name="results[{{$key}}][rslt_dt_value]" value="{{$item['value']}}"></td>
                                        <td><input type="hidden" name="results[{{$key}}][rslt_dt_alt]" value="{{$item['alt_name']}}"></td>
                                        <td><input type="hidden" name="results[{{$key}}][rslt_dt_rank]" value="{{$loop->iteration}}"></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row col-12 mt-3 ">
                            <button class=" mx-2 btn btn-secondary">Kembali</button>
                            <button class=" mx-2 btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <script>
        $(() => {
            $('#ranking').DataTable({
                searching: false,
                "ordering": false,
                info: false,
                show: false,
                "order": [[ 2, "desc" ]]
            });
        })
    </script>
@endsection
