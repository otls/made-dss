@extends('layouts.app')

@section('title')

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow"m style="min-height: 70vh">
                <div class="card-body ">

                    <div id="container" class="w-100">

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="d-none" id="cat" data-value='{{$cat}}'></div>
    <div class="d-none" id="value" data-value='{{$value}}'></div>
    <script>
        $(() => {
            let cat = JSON.parse($('#cat').attr('data-value'));
            let value = JSON.parse($('#value').attr('data-value'));
            var chart = Highcharts.chart('container', {

                title: {
                    text: 'Hasil Perhitungan Terakhir'
                },

                subtitle: {
                    text: 'Made Decision Support System'
                },

                xAxis: {
                    categories: cat
                },

                series: [{
                    type: 'column',
                    colorByPoint: true,
                    data: value,
                    showInLegend: false,
                    name: 'nilai'
                }]

            });

        })
    </script>
@endsection
