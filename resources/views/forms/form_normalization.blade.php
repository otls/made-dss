<form action="{{route('selection.update', ['selection' => $selection->id])}}" method="POST" id="form-normalization">
    @csrf
    @method('PUT')
    @foreach ($criteria as $key => $item)

        <div class="form-row mb-3">
            <div class="form-group col-12 mb-0">
                <label>{{$item->crt_name}}</label>
            </div>
            {{-- <div class="input-group col-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" value="{{$normalization[$key]['nrmlzn_label']}}" name="nrmlzn[{{$key}}][nrmlzn_label]" id="nrmlzn_label{{$item->id}}" class="form-control" required>
            </div> --}}
            <div class="input-group col-12">
                <div class="input-group-prepend">
                    <span class="input-group-text">{{$criteriaConfig[$item->crt_name][0]['unit'] ?? 'Nilai'}}</span>
                </div>
                <input min="{{$criteriaConfig[$item->crt_name][0]['min'] ?? 10}}" step=".01" type="number"value="{{$normalization[$key]['nrmlzn_label']}}"  name="nrmlzn[{{$key}}][nrmlzn_value]" id="nrmlzn_value{{$item->id}}" class="form-control" required>
            </div>
            <input type="hidden" name="nrmlzn[{{$key}}][criteria_id]" value="{{$item->id}}">
            <input type="hidden" name="nrmlzn[{{$key}}][normalization_id]" value="{{$selection->id}}">
            <input type="hidden" name="nrmlzn[{{$key}}][crt_name]" value="{{$item->crt_name}}">
        </div>
    @endforeach
    <div class="form-group mt-3">
        <button type="submit" class="btn btn-block bg-pink">SIMPAN</button>
    </div>
</form>
