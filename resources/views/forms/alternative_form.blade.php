@extends('layouts.app')

@section('title')
    @if (!isset($data->id))
        Tambah Data Alternatif
    @else
        Rubah Data Alternatif
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <form id="alt-form" action="@if(isset($data->id)) {{route('alternative.update', ['alternative' => $data->id])}} @else {{route('alternative.store')}} @endif" method="POST">
                        @csrf
                        @if (isset($data->id))
                            @method('PUT')
                        @endif
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" name="alt_name" id="alt_name" value="{{old('alt_name') ? old('alt_name') : ((isset($data->id)) ? $data->alt_name : null )}}" class="form-control @error('alt_name') is-invalid @enderror" placeholder="Masukan nama alternatif" required maxlength="100">
                            @error('alt_name')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">No. Telepon</label>
                            <input type="text" name="alt_phone" id="alt_phone" value="{{old('alt_phone') ? old('alt_phone') : ((isset($data->id)) ? $data->alt_phone : null )}}" class="form-control @error('alt_phone') is-invalid @enderror" placeholder="Masukan nomor telepon alternatif" required maxlength="100">
                            @error('alt_phone')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="alt_email" id="alt_email" value="{{old('alt_email') ? old('alt_email') : ((isset($data->id)) ? $data->alt_email : null )}}" class="form-control @error('alt_email') is-invalid @enderror" placeholder="Masukan email alternatif" required maxlength="100">
                            @error('alt_email')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea required name="alt_address" id="alt_address" class="form-control @error('alt_address') is-invalid @enderror" placeholder="Masukan alamat alternatif" cols="30" rows="5">{{old('alt_address') ? old('alt_address') : ((isset($data->id)) ? $data->alt_address : null )}}</textarea>
                            @error('alt_address')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block bg-pink">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(() => {
            $('#alt-form').validate()
        })
    </script>
@endsection
