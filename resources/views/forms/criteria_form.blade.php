@extends('layouts.app')

@section('title')
    @if (!isset($data->id))
        Tambah Data Kriteria
    @else
        Update Data Kriteria
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <form action="{{isset($data->id) ? route('criteria.update', $data->id) : route('criteria.store')}}" method="POST" id="form-criteria">
                    @csrf
                    @if (isset($data->id))
                        @method('PUT')
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label for="crt_name">Nama Kriteria</label>
                            <input type="text" value="{{old('crt_name') ? old('crt_name') : (isset($data->id) ? $data->crt_name : '')}}" placeholder="Masukan nama kriteria" name="crt_name" id="crt_name" class="form-control @error('crt_name') is-invalid @enderror" required>
                            @error('crt_name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="crt_weight">Bobot Kriteria</label>
                            <input type="number" value="{{old('crt_weight') ? old('crt_weight') : (isset($data->id) ? $data->crt_weight : '')}}" name="crt_weight" id="crt_weight" class="form-control @error('crt_weight') is-invalid @enderror" placeholder="Masukan bobot kriteria" required>
                            @error('crt_weight')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Kriteria</label>
                            <select name="crt_type" id="crt_type" class="form-control">
                                <option @if(old('crt_type') == '1' || (isset($data->id) and $data->crt_type == '1')) selected @endif value="1">Benefit</option>
                                <option @if(old('crt_type') == '0' || (isset($data->id) and $data->crt_type == '0')) selected @endif value="0">Cost</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="crt_desc">Deskripsi Kriteria <small class="text-muted">(optional)</small></label>
                            <textarea name="crt_desc" id="crt_desc" placeholder="Deskripsi kriteria" cols="30" rows="5" class="form-control @error('crt_desc') is-invalid @enderror">{{old('crt_desc') ? old('crt_desc') : (isset($data->id) ? $data->crt_desc : '')}}</textarea>
                            @error('crt_desc')
                                {{$message}}
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block bg-pink">SIMPAN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(() => {
            $('#form-criteria').validate({});
        })
    </script>
@endsection
