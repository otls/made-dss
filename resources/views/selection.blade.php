@extends('layouts.app')

@section('title')
    Pemilihan
@endsection

@section('content')
    <div class="row" id="top">
        <div class="col-12">
            <div class="card border-0 shadow w-100">
                <div class="card-body">
                    <h2 class="card-title font-weight-bold mb-3">Tabel Pencocokan</h2>
                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                                <th>Alt.</th>
                               @foreach ($criteria as $item)
                                   <th class="header-th text-center" data-name="{{strtolower(str_replace(" ", "_", $item->crt_name))}}">{{$item->crt_name}}</th>
                               @endforeach
                               <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alternatives as $item)
                                <tr>
                                    <td>{{$item->alt_name}}</td>
                                    @foreach ($item->normalization->detail as $val)
                                        <td class="text-center">{{$val->label}}</td>
                                    @endforeach
                                    <td class="tesxt-center row">
                                        <button data-name="{{$item->alt_name}}" data-id="{{$item->normalization->id}}" class="mx-1 btn btn-default btn-sm edit-normalization"><i class="fas fa-edit"></i> </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-right">
                    <form method="POST" action="{{route('count')}}" @if($zero_value > 0) onclick="(e) => e.preventDefault(); notify('Masih ada nilai nol', 'warning', 'WARNING'); return false;"  @endif>
                        @csrf
                        @method('PUT')
                        <button type="submit"  class="btn bg-pink">HITUNG</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-normalization" tabindex="-1" role="dialog" aria-labelledby="modal-normalizationTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-body">

                </div>
            </div>
        </div>
    </div>
    <script>
        $(() => {
            $('button.edit-normalization').click(e => {
                let id = $(e.currentTarget).attr('data-id');
                let url = `{{route('selection.edit', ':id')}}`.replace(':id', id);
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {},
                    dataType: "json",
                    success: function (response) {
                        $("#modal-body").html(response.data);
                        $('#modal-title').html($(e.currentTarget).attr('data-name'));
                        $('#modal-normalization').modal('show');
                    },
                    error: (xhr, error, status) => {
                        notify(xhr.responseJSON.message, 'error', error);
                    }
                });
            });
        });
    </script>
@endsection
