require('./bootstrap');

// notification toastr
window.notify = (message = "Something happened!", type = "info", title = "INFO") => {
    switch (type) {
        case 'success':
            toastr.success(message, title);
            break;
        case 'info':
            toastr.info(message, title);
            break;
        case 'warning':
            toastr.warning(message, title);
            break;
        case 'error':
            toastr.error(message, title);
            break;
        default:
            toastr.info(message, title);
            break;
    }
}

window.setFormDeleteConfirmListener = () => {

    $('button.delete-confirm').click(function (event) {
        event.preventDefault();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: 'Data akan dihapus secara permanent',
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, hapus!'
        }).then(function (value) {
            if (value.value) {
                $('form.delete-confirm').submit();
            }
        });
    });
}

window.Highcharts = require('highcharts/highstock');
// Load Highcharts Maps as a module
require('highcharts/modules/map')(window.Highcharts);
