<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $fillable = [
        'alt_name',
        'alt_email',
        'alt_phone',
        'alt_address',
    ];

    public function normalization()
    {
        return $this->hasOne(Normalization::class, 'alternative_id');
    }
}
