<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultDetail extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $appends = ['color'];

    public function details()
    {
        return $this->belongsTo(Result::class, 'result_id');
    }

    public function getColorAttribute()
    {
        return substr(md5($this->id . $this->created_at . $this->updated_at), 0, 6);
    }
}
