<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $appends = ['type'];

    public function getTypeAttribute()
    {
        return $this->crt_type == '1' ? 'Benefit' : 'Cost';
    }
}
