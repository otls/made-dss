<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Normalization extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function detail()
    {
        return $this->hasMany(NormalizationDetail::class, 'normalization_id');
    }

    public function alternative()
    {
        return $this->belongsTo(Alternative::class, 'alternative_id');
    }

    // public function scopeFormattedDetail($q)
    // {
    //     $data = [];
    //     foreach ($q->detail as $key) {
    //     }
    // }
}
