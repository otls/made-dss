<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NormalizationDetail extends Model
{
    protected $fillable = ['criteria_id', 'alternative_id', 'nrmlzn_label', 'nrmlzn_value'];
    protected $appends = ['label'];

    public function normalization()
    {
        return $this->belongsTo(Normalization::class, 'normalization_id');
    }

    public function criteria()
    {
        return $this->belongsTo(Criteria::class, 'criteria_id');
    }

    function getLabelAttribute(){
        $crtConfig = config('made_dss.criteria');
        if ($this->nrmlzn_value > 0) {
            return $this->nrmlzn_label . " " . $crtConfig[$this->criteria->crt_name][0]['unit'];
        }
        return '-';
    }
}
