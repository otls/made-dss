<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlternativeStore;
use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\Normalization;
use App\Models\NormalizationDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AlternativeController extends AppController
{
    private $controllerSubject = "Alternative";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('alternatives');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.alternative_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlternativeStore $request)
    {
        $data = $request->except('_token');
        try {
            DB::transaction(function () use ($data) {
                $alt_id = Alternative::insertGetId($data);
                $nrmlzn = null;
                if (!$nrmlzn = Normalization::where('alternative_id', $alt_id)->first()) {
                    $nrmlzn = Normalization::insertGetId(['alternative_id' => $alt_id]);
                } else {
                    $nrmlzn = $nrmlzn->id;
                }

                $nrmlzn_crt = NormalizationDetail::where('normalization_id', $nrmlzn)
                    ->get('criteria_id')
                    ->toArray();
                $criterias = Criteria::whereNotIn('id', $nrmlzn_crt)->get();
                $data_nrmlzn_dt = [];
                if ($criterias) {
                    foreach ($criterias as $key => $value) {
                        $data_nrmlzn_dt[] = [
                            'normalization_id' => $nrmlzn,
                            'criteria_id' => $value['id'],
                            'nrmlzn_value' => 0,
                            'nrmlzn_label' => '-',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    NormalizationDetail::insert($data_nrmlzn_dt);
                }
            });
            $this->response = $this->inputSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->inputFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('alternative.index')->with($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Alternative $alternative)
    {
        return view('forms.alternative_form', ['data' => $alternative]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlternativeStore $request, Alternative $alternative)
    {
        try {
            $alternative->alt_name = $request->alt_name;
            $alternative->alt_email = $request->alt_email;
            $alternative->alt_phone = $request->alt_phone;
            $alternative->alt_address = $request->alt_address;
            $alternative->save();
            $this->response = $this->updateSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->updateFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('alternative.index')->with($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alternative $alternative)
    {
        try {
            $alternative->delete();
            $this->response = $this->deleteSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->deleteFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('alternative.index')->with($this->response);
    }

    public function data(Request $request)
    {
        $data = Alternative::orderBy('alt_name', 'asc');

        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->toJson();
    }
}
