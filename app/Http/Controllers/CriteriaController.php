<?php

namespace App\Http\Controllers;

use App\Http\Requests\CriteriaStore;
use App\Models\Criteria;
use App\Models\NormalitationDetail;
use App\Models\Normalization;
use App\Models\NormalizationDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CriteriaController extends AppController
{
    private $controllerSubject = "Kriteria";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crieria');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.criteria_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CriteriaStore $request)
    {
        $data = $request->except('_token');
        try {
            DB::transaction(function () use ($data) {
                $crt_id = Criteria::insertGetId($data);
                $nrmlzn_dt_ids = Normalization::whereDoesntHave('detail', function ($q) use ($crt_id) {
                    $q->where('normalization_details.criteria_id', $crt_id);
                })->get('id');
                if ($nrmlzn_dt_ids) {
                    $nrmlzn_dt_data = [];
                    foreach ($nrmlzn_dt_ids as $key => $value) {
                        $nrmlzn_dt_data[] = [
                            'normalization_id' => $value['id'],
                            'criteria_id' => $crt_id,
                            'nrmlzn_value' => 0,
                            'nrmlzn_label' => "-",
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    NormalizationDetail::insert($nrmlzn_dt_data);
                }
            });
            $this->response = $this->inputSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->inputFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('criteria.index')->with($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Criteria $criterion)
    {
        return view('forms.criteria_form', ['data' => $criterion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CriteriaStore $request, Criteria $criterion)
    {
        try {
            $criterion->crt_name = $request->crt_name;
            $criterion->crt_weight = $request->crt_weight;
            $criterion->crt_desc = $request->crt_desc;
            $criterion->crt_type = $request->crt_type;
            $criterion->save();
            $this->response = $this->updateSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->updateFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('criteria.index')->with($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Criteria $criterion)
    {
        try {
            $criterion->delete();
            $this->response = $this->deleteSuccess($this->controllerSubject);
        } catch (\Exception $e) {
            $this->response = $this->deleteFailed($this->controllerSubject, $e->getMessage());
        }
        return redirect()->route('criteria.index')->with($this->response);
    }
    public function data(Request $request)
    {
        if ($request->ajax()) {
            $criterias = Criteria::query();
            return DataTables::eloquent($criterias)
                ->addIndexColumn()
                ->toJson();
        }
    }
}
