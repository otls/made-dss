<?php

namespace App\Http\Controllers;

use App\Models\Result;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $results = Result::orderBy('created_at', 'desc')->first();
        $value = "[]";
        $cat = "[]";
        $result_details = "[]";
        if ($results) {
            # code...
            $result_details = $results->details;
            $cat = json_encode($result_details->pluck('rslt_dt_alt'));
            $value = json_encode($result_details->pluck('rslt_dt_value'));
        }
        $data = [
            'results' => $results,
            'result_details' => $result_details,
            'cat' => $cat,
            'value' => $value,
        ];

        return view('home', $data);
    }
}
