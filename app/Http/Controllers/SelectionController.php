<?php

namespace App\Http\Controllers;

use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\Normalization;
use App\Models\NormalizationDetail;
use App\Models\Result;
use App\Models\ResultDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class SelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'zero_value' => NormalizationDetail::where('nrmlzn_value', '=', 0)->count(),
            'criteria' => Criteria::orderBy('id', 'asc')->get(),
            'alternatives' => Alternative::whereHas('normalization')->get(),
            'criteriaConfig' => collect(config('made_dss.criteria'))
        ];
        return view('selection', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $criteria = Criteria::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Normalization $selection)
    {
        if ($request->ajax()) {
            try {
                $criteriaConfig = collect(config('made_dss.criteria'));
                $criteria = Criteria::orderBy('id', 'asc')->get();
                $normalization = $selection->detail->sortBy('id')->toArray();
                $page = view('forms.form_normalization', compact(['criteria', 'normalization', 'selection', 'criteriaConfig']))->render();
                $response = [
                    'code' => 200,
                    'data' => $page
                ];
            } catch (\Exception $e) {
                $response = [
                    'code' => 500,
                    'message' => $e->getMessage()
                ];
            }
            return response()->json($response, $response['code']);
        } else {
            return redirect()->route('selection.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Normalization $selection)
    {
        if ($data = $request->input('nrmlzn')) {
            try {
                $criteriaConfig = config('made_dss.criteria');
                foreach ($data as $key => $value) {
                    $crt = isset($criteriaConfig[$value['crt_name']]) ? $criteriaConfig[$value['crt_name']] : abort(400);
                    $crt = collect($crt);
                    $tmpVal = floatval($value['nrmlzn_value']);
                    $tmpCrt = $crt->where('min', '<=', $tmpVal)->where('max', '>=', $tmpVal)->first();
                    $dt_normalization = [
                        'nrmlzn_value' => $tmpCrt['value'] ?? 0,
                        'nrmlzn_label' => $value['nrmlzn_value'],
                        'normalization_id' => $selection->id,
                        'criteria_id' => $value['criteria_id']
                    ];
                    NormalizationDetail::updateOrCreate(
                        ['normalization_id' => $selection->id, 'criteria_id' => $value['criteria_id']],
                        $dt_normalization
                    );
                }
                // dd($dt_normalization);
                $data = [
                    'criteria' => Criteria::orderBy('id', 'asc')->get(),
                    'alternatives' => Alternative::whereHas('normalization')->get()
                ];
                $response = [
                    'code' => 200,
                    'message' => "Berhasil merubah data",
                ];
            } catch (\Exception $e) {
                $response = [
                    'code' => 500,
                    'message' => $e->getMessage()
                ];
            }
            return redirect()->route('selection.index')->with($response);
        } else {
            return redirect()->route('selection.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function count(Request $request)
    {
        $vals = NormalizationDetail::selectRaw('criteria_id, SQRT(sum(POWER(nrmlzn_value, 2))) AS val')->groupBy('criteria_id')->get();
        $normalization = Normalization::get();
        $data = [];
        $normalizedMatrix = [];
        $criteria = Criteria::get();
        $alternatives = Alternative::get();
        $optimasi = 0;

        foreach ($normalization as $key => $value) {
            $data[$key]['alt_name'] = $value->alternative->alt_name;
            $data[$key]['alt_id'] = $value->alternative->id;
            $max = 0;
            $min = 0;
            foreach ($value->detail as $k => $item) {
                $mtx_val = number_format($item->nrmlzn_value / $vals[$k]->val, 4);
                // dd($mtx_val);
                $normalizedMatrix[$value->alternative->alt_name][] = $mtx_val;
                if ($item->criteria->crt_type == '1') {
                    $max += ($mtx_val * $item->criteria->crt_weight);
                    // $dataTest[$value->alternative->alt_name]['max'][$item->criteria->crt_name] = ($mtx_val * $item->criteria->crt_weight);
                } else {
                    $min += $mtx_val * $item->criteria->crt_weight;
                    // $dataTest[$value->alternative->alt_name]['min'][$item->criteria->crt_name] = ($mtx_val * $item->criteria->crt_weight);
                }
            }
            $optimasi = $max - $min;
            // $dataTest[$value->alternative->alt_name]['optimasi'][] = $optimasi;
            // dd($dataTest);
            $data[$key]['value'] = $optimasi;
        }
        // dd($data);
        $ranking = collect($data);
        $ranking = $ranking->sortBy('value')->all();
        return view('count_result', compact(['criteria', 'alternatives', 'normalizedMatrix', 'data', 'ranking']));
    }

    public function save_results(Request $request)
    {
        // return $request->input();
        $request->validate([
            'results' => 'array',
            'results.*' => ['array'],
        ]);

        try {
            $result_detail = $request->except(['_token']);
            // dd($result_detail['results']);
            DB::transaction(function () use ($request, $result_detail) {
                $result = Result::create(['result_periode' => Carbon::now()]);
                foreach ($result_detail['results'] as $key => $item ) {
                    $item['result_id'] = $result->id;
                    ResultDetail::create($item);
                }

            });

            $data = ['message' => 'Berhasil menyimpan', 'type' => 'success'];

        } catch (\Exception $th) {
            $data = ['message' => $th->getMessage(), 'type' => 'error'];
        }

        return redirect()->route('selection.index')->with($data);
    }
}
