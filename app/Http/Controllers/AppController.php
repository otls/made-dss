<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AppController extends Controller
{
    // Response properties
    protected $data = [];
    protected $code = 200;
    protected $message = "Operation success";
    protected $title = "INFO";
    private $type = "info";
    protected $response = ['success' => true, 'message' => 'Operation Success', 'code' => 200, 'type' => 'success'];

    /*----------------------------------------------------------------------------------------------//
    * CRUD RESPONSES
    -----------------------------------------------------------------------------------------------*/
    protected function inputSuccess($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Berhasil menambahkan {$subject}!" : $message;
        $this->title = "Input Berhasil";
        $this->type = 'success';

        return $this->setMessage();
    }
    protected function inputFailed($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Gagal menambahkan {$subject}!" : $message;
        $this->title = "Input Gagal";
        $this->type = 'error';

        return $this->setMessage();
    }
    protected function updateSuccess($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Berhasil merubah {$subject}!" : $message;
        $this->title = "Update Berhasil";
        $this->type = 'success';

        return $this->setMessage();
    }
    protected function updateFailed($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Gagal merubah {$subject}!" : $message;
        $this->title = "Update Gagal";
        $this->type = 'error';

        return $this->setMessage();
    }
    protected function deleteFailed($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Gagal memnghapus {$subject}!" : $message;
        $this->title = "Hapus Gagal";
        $this->type = 'error';

        return $this->setMessage();
    }
    protected function deleteSuccess($subject = "data", $message = false, $code = 200, $data = [])
    {
        $this->subject = $subject;
        $this->code = $code;
        $this->data = $data;
        $this->message = !$message ? "Berhasil memnghapus {$subject}!" : $message;
        $this->title = "Hapus Berhasil";
        $this->type = 'success';

        return $this->setMessage();
    }
    protected function setMessage()
    {
        return [
            'type' => $this->type,
            'title' => $this->title,
            'message' => $this->message,
            'data' => $this->data,
            'code' => $this->code
        ];
    }
    /*--END CRUD RESPONSE----------------------------------------------------------------------------*/

    protected function datatable($eloqunet)
    {
        return DataTables::eloquent($eloqunet)->toJson();
    }
}
