<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AlternativeStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alt_name' => 'required|string|max:255',
            'alt_phone' => 'required|string|max:20',
            'alt_email' => ['required', 'string', 'max:100', Rule::unique('alternatives')->ignore($this->alternative)],
            'alt_address' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'alt_name' => 'Alternative',
            'alt_phone' => 'Phone',
            'alt_email' => 'Email',
            'alt_address' => 'Address',
        ];
    }
}
