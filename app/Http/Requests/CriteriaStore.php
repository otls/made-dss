<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class CriteriaStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crt_name' => ['required', 'string'],
            'crt_weight' => ['required', 'numeric'],
            'crt_desc' => ['nullable', 'string'],
            'crt_type' => ['required', 'boolean'],
        ];
    }

    public function attributes()
    {
        return [
            'crt_name' => 'Criteria',
            'crt_weight' => 'weight',
            'crt_desc' => 'Description',
            'crt_type' => "Criteria type"
        ];
    }
}
